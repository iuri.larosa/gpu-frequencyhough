# GPU-FrequencyHough

This repository contains all the codes and documentation of the GPU enabled version of the FrequencyHough CW search algorithm. To see them from the website just explore the branches. To see them locally, after having cloned the repository, run git checkout BRANCHNAME.
